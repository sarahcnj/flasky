from flask import Flask, jsonify
import os

app = Flask(__name__)

basedir = os.path.abspath(os.path.dirname(__file__))



@app.route('/')
def home():
    return jsonify(data='My MLE02 Home Page - Welcome!!!')



if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000)


